const theme = process.env.WP_THEME;

module.exports = {
  purge: [
    `./public/themes/${theme}/**/*.php`,
    `./resources/styles/**/*.scss`
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      container: {
        center: true,
        padding: {
          DEFAULT: '1rem',
          '2xl': 0
        }
      },
      fontFamily: {
        sans: ['PT Sans Caption', 'sans-serif'],
        serif: ['Redaction Regular'],
        serifBold: ['Redaction Bold'],
      },
      colors: {
        blue: '#25317E',
        yellow: '#EDD028',
        vermillon: '#DC3319',
        mandarin: '#EE733A'

      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
