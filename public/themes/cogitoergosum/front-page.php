<?php
get_header();
$values = array(
    'Architecture', 'Urbanisme', 'Environnement', 'Aménagement',
    'Économie de la construction', 'Coordination & Pilotage de chantier'
);
?>

<div class="h-screen w-full bg-blue relative flex flex-col justify-center items-center">
    <img class="fixed h-32 top-0 left-0 transform rotate-90 lg:top-auto lg:rotate-0" src="<?= get_template_directory_uri(); ?>/assets/images/plant-01.svg" alt="">
    <h1 class="sr-only">Cogito Ergo Sum</h1>
    <div class=" mt-auto container lg:flex justify-center space-y-6 lg:space-y-0 lg:space-x-12">
        <h2 class="text-4xl xl:text-5xl text-white max-w-md pb-6 lg:pb-0 border-b lg:border-b-0 lg:border-r border-yellow">Psycho-sociologie de l'architecture</h2>
        <ul class="text-white">
            <?php foreach ($values as $v) : ?>
                <li><?= $v; ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
    <a href="https://cogitoergosum.dev/cogito-ergo-sum/" aria-label="Aller à l'accueil" class="transform -translate-y-14 mt-auto w-14 h-14 flex place-items-center justify-center rounded-full border text-yellow border-yellow">
        <svg role="img" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24" width="24" height="24">
            <path d="M12 13.172l4.95-4.95 1.414 1.414L12 16 5.636 9.636 7.05 8.222z" />
        </svg>
    </a>
</div>

<?php
get_footer();
?>