<?php
get_header();

$projects = get_posts(array(
    'post_type' => 'projects',
    'numberposts' => -1,
    'orderby' => 'menu_order'
));

?>

<main role="main" class="lg:pl-14 py-4 lg:py-10">
    <div class="container">
        <h1>Nos projets</h1>
        <div class="grid md:grid-cols-2 lg:grid-cols-3">
            <?php if ($projects) :
                foreach ($projects as $project) :
                    setup_postdata($post); ?>

                    <?php the_title('<h2>', '</h2>'); ?>

            <?php endforeach;
            else : ?>
            <p class="text-xl pt-6 text-gray-600">Aucun projet n'a été trouvé.</p>
            <?php
            endif;
            wp_reset_postdata(); ?>
        </div>
    </div>
</main>

<?php
get_footer();
