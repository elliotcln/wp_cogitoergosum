<?php

/**
 * Register & load scripts & styles
 */
add_action('wp_enqueue_scripts', function () {
    wp_register_style('fonts', 'https://fonts.googleapis.com/css2?family=PT+Sans+Caption:wght@400;700&display=swap');
    wp_register_style('style', get_template_directory_uri() . '/style.css');
    wp_deregister_script('jquery');


    // enqueue
    wp_enqueue_style('fonts');
    wp_enqueue_style('style');
});

/**
 * Set theme supports
 */
add_action('after_setup_theme', function () {
    add_theme_support('post-thumbnails');
    add_theme_support('title-tag');

    register_nav_menus([
        'navigation' => __('Navigation'),
    ]);
});

/**
 * Remvove unused links into admin navbar
 */
function wp_remove_admin_links()
{
    remove_menu_page('edit-comments.php');
}
add_action('admin_init', 'wp_remove_admin_links');

add_action('init', function () {

    register_post_type('projects', array(
        'labels' => [
            'name' => __('Projets'),
            'singular_name' => __('Projet'),
            'all_items' => __('Tous les projets'),
            'add_new' => __('Ajouter un projet')
        ],
        'taxonomies' => array('post_tag'),
        'menu_icon' => 'dashicons-art',
        'public' => true,
        'has_archives' => true,
        'rewrite' => array('slug' => 'projet'),
        'show_in_rest' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'supports' => ['title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', 'page-attributes']
    ));
    
});

/**
 * Define navigation walker
 */
class Main_Nav_Walker extends Walker_Nav_Menu
{

    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {
        $indent = ($depth) ? str_repeat("\t", $depth) : '';

        $class_names = $value = '';

        $classes = empty($item->classes) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;

        $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args));
        if ($args->walker->has_children) {
            $class_names .= ' group';
        }
        $class_names = $class_names ? ' class="' . esc_attr($class_names) . '"' : '';

        $id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args);
        $id = $id ? ' id="' . esc_attr($id) . '"' : '';

        $output .= $indent . '';

        $attributes  = !empty($item->attr_title) ? ' title="'  . esc_attr($item->attr_title) . '"' : '';
        $attributes .= !empty($item->target)     ? ' target="' . esc_attr($item->target) . '"' : '';
        $attributes .= !empty($item->xfn)        ? ' rel="'    . esc_attr($item->xfn) . '"' : '';
        $attributes .= !empty($item->url)        ? ' href="'   . esc_attr($item->url) . '"' : '';
        if (in_array('current-menu-item', $item->classes)) {
            $attributes .= 'aria-current="page"';
        }

        $item_output = $args->before;
        $item_output .= '<li' . $class_names . '>';
        $item_output .= '<a' . $attributes . ' class="flex-grow">';
        $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
        $item_output .= '</a>';
        if ($args->walker->has_children) {
            $item_output .= '<label role="button" for="check-menu-item-parent-' . $item->ID . '" class="ml-auto lg:ml-1 w-7 h-7 flex place-items-center justify-center">
                <svg fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 15l-4.243-4.243 1.415-1.414L12 12.172l2.828-2.829 1.415 1.414z"/></svg>
            </label>';
        }
        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
        $item_output .= $args->after;
        if ($args->walker->has_children) {
            $output .= '<input type="checkbox" class="hidden" id="check-menu-item-parent-' . $item->ID . '">';
        }
        $item_output .= '</li>';
    }
}
