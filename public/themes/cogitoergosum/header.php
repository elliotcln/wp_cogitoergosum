<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <?php

  if(!is_front_page()) : ?>
  <div class="hidden font-serif lg:block fixed bottom-0 left-5 text-sm text-gray-400">
    <span class="inline-block origin-top-left transform -rotate-90"><?php bloginfo('description'); ?></span>
  </div>
  <header class="site-header">
    <div class="2xl:container lg:flex lg:justify-between lg:place-items-center">
      <div class="brand px-5 py-3 flex place-items-center justify-between">
        <a href="<?php bloginfo('url'); ?>">
          <img class="h-20 lg:h-auto" src="<?= get_template_directory_uri(); ?>/assets/images/logo_cogito-ergo-sum.svg" alt="Cogito Ergo Sum - Logo">
        </a>
        <label for="toggle-main-navigation" role="button" class="w-7 h-7 flex place-items-center justify-center lg:hidden">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
            <path fill="none" d="M0 0h24v24H0z" />
            <path fill="currentColor" d="M3 4h18v2H3V4zm0 7h18v2H3v-2zm0 7h18v2H3v-2z" />
          </svg>
        </label>
      </div>

      <input type="checkbox" id="toggle-main-navigation" class="hidden">
      <nav class="lg:mt-3 px-5 main-navigation" role="navigation" aria-label="Navigation principale">
        <ul class="lg:text-sm xl:text-base lg:flex lg:space-x-6 lg:items-center">
          <?php wp_nav_menu([
            'theme_location' => 'navigation',
            'items_wrap' => '%3$s',
            'container' => null,
            'walker' => new Main_Nav_Walker()
          ]); ?>
        </ul>
      </nav>
    </div>
  </header>
  <?php endif; ?>