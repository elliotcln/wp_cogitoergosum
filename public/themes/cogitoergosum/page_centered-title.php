<?php
/*
 * Template Name: Center Title Template
 */

get_header();
?>

<main role="main" class="lg:pl-14 py-4 lg:py-10">
    <div class="container">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <article>
                    <header class="text-center mb-4 lg:mb-8">
                        <?php the_title('<h1>', '</h1>'); ?>
                        <svg class="inline-block w-9 text-yellow" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                            <path d="M344,310.5Q289,371,181,375Q73,379,105,273.5Q137,168,213,149Q289,130,344,190Q399,250,344,310.5Z" fill="currentColor"></path>
                        </svg>
                    </header>
                    <?php if (has_post_thumbnail()) : ?>
                        <figure class="thumbnail mb-4 lg:mb-8">
                            <?php the_post_thumbnail(); ?>
                        </figure>
                    <?php endif; ?>
                    <main class=" max-w-3xl lg:py-8 mx-auto">
                        <?php the_content(); ?>
                    </main>
                </article>
        <?php endwhile;
        endif; ?>
    </div>
</main>

<?php
get_footer();
